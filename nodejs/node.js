const http = require('node:http');

const hostname = '0.0.0.0';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello from NodeJS!');
});

server.listen(port, hostname, () => { 
  console.log(`Server running at http://${hostname}:${port}/`);
}); 

// const express = require('express')
// const app = express()
// const os = require('os');
// const port = 5000

// app.get('/', (req, res) => {
//     res.send(`<h3>It's ${os.hostname()}</h3>`);
// })
// app.listen(port, () => {
//     console.log(`Server Started on Port  ${port}`)
// })