
# Express JS

mkdir express && cd express

nano express.js
const express = require('express')
const app = express()
const os = require('os');
const port = 5000
app.get('/', (req, res) => {
  res.send(`Hello from an Express container - ${os.hostname()}`)
})
app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

nano Dockerfile
FROM node:18-alpine
RUN addgroup -S docker && adduser -S -G docker docker
RUN apk add curl --no-cache
WORKDIR /app
COPY ./express.js ./express.js
RUN npm init -y && npm install express
USER docker
EXPOSE 5000
CMD ["node","express.js"]

docker build -t express .
docker run -d -P express

docker-compose rm -f
docker-compose up -d
docker-compose down

Fire-Guard Install
------------------------

# Prepare DNS - https://wb.nakodevopsprojects.store

# Generate SSL (certbot, let's encrypt)
apt update
snap install core; snap refresh core
snap install --classic certbot
ln -s /snap/bin/certbot /usr/bin/certbot
certbot certonly --standalone

# Install fireguard manual
sudo apt update && sudo apt install wireguard -y
curl -1sLf 'https://dl.cloudsmith.io/public/firezone/firezone/setup.deb.sh' | sudo -E bash
sudo apt install firezone -y

# firezone-ctl reconfigure
# nano /etc/firezone/firezone.rb 

# default['firezone']['external_url'] = 'https://frg.nakodevopsprojects.store'
# default['firezone']['ssl']['email_address'] = 'mr.nako@yahoo.com'

# default['firezone']['external_url'] = 'https://18.119.121.97'
# default['firezone']['ssl']['email_address'] = 'mr.nako@yahoo.com'

# default['firezone']['ssl']['certificate'] = '/etc/letsencrypt/live/frg.nakodevopsprojects.store/fullchain.pem'
# default['firezone']['ssl']['certificate_key'] = '/etc/letsencrypt/live/frg.nakodevopsprojects.store/privkey.pem'

sudo firezone-ctl reconfigure
sudo firezone-ctl create-or-reset-admin

https://frg.nakodevopsprojects.store
https://3.142.124.112

# Install fireguard docker
mkdir firezone && firezone

# Postgres Environment File
POSTGRES_PASSWORD=YOUR_PASSWORD_HERE
POSTGRES_DB=firezone
POSTGRES_USER=postgres

# Generate a Default Environment File
docker run --rm firezone/firezone bin/gen-env > container-vars.env

nano container-vars.env
----------------------------

# Postgres
DATABASE_HOST=firezone-postgres
DATABASE_NAME=firezone
DATABASE_USER=postgres
DATABASE_PASSWORD=YOUR_PASSWORD_HERE      # same password as in container-vars-postgres.env

# Firezone web server
EXTERNAL_URL=https://18.118.37.85

# Firezone admin setup
DEFAULT_ADMIN_EMAIL=asdasd@gmail.com
DEFAULT_ADMIN_PASSWORD=dasdasdasdasdsada

# Secrets
GUARDIAN_SECRET_KEY=6IWI6E0zavQcGrxCuS8NW3RbRfQR5RrrBfGNyGPndqtiW+o/ZX5vRbvfUYWoHl3c
DATABASE_ENCRYPTION_KEY=baBSw99Td6nUIxO2fBnJ1LY9voGYLcZR/D1+XwL9F8A=
SECRET_KEY_BASE=DAaiHuRtRV3AMQFG+XpN/RTKcfTHEqlFPsbu3x25Onl5I7WEOWhUtmrRg9Fyam44
LIVE_VIEW_SIGNING_SALT=HZstmlqPb4dtq73NAcjSVv8dXcqbM40K
COOKIE_SIGNING_SALT=0w6r8FxV
COOKIE_ENCRYPTION_SALT=Vm1IvKZ2

# Firezone misc.
TELEMETRY_ENABLED=false

# Email
OUTBOUND_EMAIL_FROM="asdasd@gmail.com"  # replace with your name/email
OUTBOUND_EMAIL_ADAPTER=Elixir.Swoosh.Adapters.Sendgrid
OUTBOUND_EMAIL_ADAPTER_OPTS={"api_key": "YOUR SENDGRID API KEY"}

# WireGuard network settings copied from default.env.
# Firezone recommends NOT changing these.
WIREGUARD_IPV4_NETWORK=100.64.0.0/10
WIREGUARD_IPV4_ADDRESS=100.64.0.1
WIREGUARD_IPV6_NETWORK=fd00::/106
WIREGUARD_IPV6_ADDRESS=fd00::1

# Database Initialization
docker compose run --rm firezone bin/migrate

# Create the first user:
docker compose run --rm firezone bin/create-or-reset-admin

# Bring the services up
docker compose up -d

=============================
# Resources
https://wiki.crowncloud.net/?How_to_Install_Firezone_UI_for_WireGuard_VPN_on_Ubuntu_22_04 
https://www.firezone.dev/docs/deploy/omnibus
https://wiki.iphoster.net/wiki/WireGuard_-_firezone_-_%D1%83%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0_%D0%BD%D0%B0_Ubuntu_20.04_-_Focal_Fossa



http://3.135.249.187:8082

# SWARM

docker swarm init
docker stack deploy -c docker-compose.yml swarmnodeapp
docker stack rm swarmnodeapp
docker service ls
docker service ps
docker container ls
docker service scale swarmnodeapp_nodeapp=5

# SSL certificate generate with certbot 

apt update
apt install certbot -y
certbot certonly --standalone
certbot certificates
harbor.dvsp.online


# curl -I https://harbor.dvsp.online

=========================

# NGINX

mkdir -p docker/nginx && cd docker/nginx

# генерируем сертификат для nginx

apt update
apt install certbot -y
certbot certonly --standalone
# wb.dvsp.online
certbot certificates

cp /etc/letsencrypt/live/wb.dvsp.online/fullchain.pem ~/docker/nginx/fullchain.pem
cp /etc/letsencrypt/live/wb.dvsp.online/privkey.pem ~/docker/nginx/privkey.pem

docker build -t mynginx .
docker run -p 80:80 -p 443:443 -v ./nginx.conf:/etc/nginx/nginx.conf -d mynginx 

docker run -p 80:80 -v ./nginx.conf:/etc/nginx/nginx.conf --network=6docker_frontnet -d nginx   
docker run -p 80:80 -v ./nginx.conf:/etc/nginx/nginx.conf -d nginx   
docker network connect net cont

docker run -d -p 4000:3000 harbor.dvsp.online/docker/nodejs:V1


всё очень просто,достаточно ,идём по ссылке https://cf-warp.maple3142.net/
полученный конфиг активируем в Network Manager

CONF_FILE="wb.conf"
nmcli connection import type wireguard file "$CONF_FILE"

Connection 'wb' (125d4b76-d230-47b0-9c31-bb7b9ebca861) successfully added.
затем разрываем соединение

nmcli connection down wb
service network-manager restart

ждём секунд 5 ,и

nmcli connection up wb
вот и всех делов



[Interface]
PrivateKey = lIvlO/yoE3CvzVPaOZUwKWuyVnSTH/M9wpFYeonfdw0=
Address = 192.168.90.95/32,fd00::26:ab9d/128
MTU = 1500
DNS = 10.7.1.6,web-bee.loc,svc.cluster.local,betiz.loc

[Peer]
PresharedKey = 1RsLbsFOixa0foNkq1rfDj0WW5wcKCFeRaC1PPiWHog=
PublicKey = HXOJeSigFBk4upSZXI4Ez3XGdOLZVL7gN8M3pIYBgAY=
AllowedIPs = 10.7.1.0/24,192.168.50.0/24,172.30.0.0/16,10.7.50.0/24
Endpoint = firezone.moskit.pro:22223
PersistentKeepalive = 25














