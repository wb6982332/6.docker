# Step 1

# install docker & compose

#!/bin/bash
sudo apt update && sudo apt install ca-certificates curl gnupg lsb-release -y
sudo mkdir -m 0755 -p /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o docker.gpg
sudo mv docker.gpg /etc/apt/keyrings/docker.gpg
sudo echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | tee docker.list
sudo mv docker.list /etc/apt/sources.list.d/docker.list
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y
sudo curl -SL https://github.com/docker/compose/releases/download/v2.20.2/docker-compose-linux-x86_64 -o /usr/bin/docker-compose
sudo chmod 777 /usr/bin/docker-compose
sudo systemctl enable docker
sudo usermod -aG docker ${USER}
sudo systemctl status docker

which docker
which compose
docker compose version
docker image ls

# Step 2

# HARBOR Registry

# генерируем сертификат для домена Harb

apt update
apt install certbot -y
certbot certonly --standalone
certbot certificates
harbor.dvsp.online

# Установка Harbor

mkdir -p /opt/harbor
mkdir -p /opt/harbor/SSL
mkdir -p /opt/harbor/data
cp /etc/letsencrypt/live/harbor.dvsp.online/fullchain.pem /opt/harbor/SSL
cp /etc/letsencrypt/live/harbor.dvsp.online/privkey.pem /opt/harbor/SSL
wget https://github.com/goharbor/harbor/releases/download/v2.8.3/harbor-online-installer-v2.8.3.tgz
tar xvxf harbor-online-installer-v2.8.3.tgz -C /opt/
cd /opt/harbor/
cp harbor.yml.tmpl harbor.yml

# добавляем свой данные

nano harbor.yml
hostname: harbor.dvsp.online
certificate: /opt/harbor/SSL/fullchain.pem
private_key: /opt/harbor/SSL/privkey.pem
harbor_admin_password: admin123
password: admin123
data_volume: /opt/harbor/data

# устанавливаем harbor

sudo ./install.sh --with-trivy --with-notary

# Step 3

# NodeJS

mkdir -p docker/nodejs && cd docker/nodejs

nano node.js

const http = require('node:http');
const hostname = '0.0.0.0';
const port = 3000;
const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('Hello from NodeJS!');
});
server.listen(port, hostname, () => { 
  console.log(`Server running at http://${hostname}:${port}/`);
}); 


nano Dockerfile

FROM node:18-alpine
RUN apk add curl --no-cache
RUN addgroup -S docker && adduser -S -G docker docker
WORKDIR /app
COPY ./node.js ./node.js
USER docker
EXPOSE 3000
CMD ["node","node.js"]

# Устанавливаем полезный инструмент Dockle для проверки образов и бест практис
VERSION=$(curl --silent "https://api.github.com/repos/goodwithtech/dockle/releases/latest" | 
 grep '"tag_name":' | sed -E 's/.*"v([^"]+)".*/\1/' \
) && curl -L -o dockle.deb https://github.com/goodwithtech/dockle/releases/download/v${VERSION}/dockle_${VERSION}_Linux-64bit.deb

sudo dpkg -i dockle.deb && rm dockle.deb

# Build image
docker build -t harbor.dvsp.online/docker/nodejs:V1 .
# docker run -d -P nodejs:latest 

# проверка с dockle 
dockle harbor.dvsp.online/docker/nodejs:V1

# Пуш к Harbor
docker login harbor.dvsp.online
docker push harbor.dvsp.online/docker/nodejs:V1

# Step 4






